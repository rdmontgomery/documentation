# Cryzen documentation

Cryzen trading platform documentation and example code.

# Manual

## General code structure

Every algorithm in Cryzen code studio consits of the definitions of two functions `setup(context)` and `run(data)`.
We will explain their usage in the following.

### Setup function
Setup function is called at the start of the algorithm and sets up the enviroment in which the main code is executed.
It allows user to pick the data streams, and, in case of backtesting, desired initial conditions.

```python
def setup(context):
    context.attribute_1 = ...
    context.attribute_2 = ...
    ...
```
The following attributes **must** be defined both for live and backtest mode
1. `context.exchanges = {exchange_name: symbols_list}` - defines the exchanges and cryptocurrency pairs that the user wants to have access to. 
Up to two cryptocurrency pairs are supported at the moment. Example values:
    *  `exchange_name = 'binance'`
    *  `symbols_list = ['BTC/USDT']`
2. `context.accounts` a dictionary with keys given by account names
    and the following entries in the dictionary
    * `id` (**always**) the identifier of the exchane
    * `balance` (**backtest**) a dictionary of starting balances
    Note that the container will ask for keys / secrets for every account specified.
3. `context.on_error` TODO

The following attributes **must** be defined for backtest mode only
* `context.start = '2018-10-20T10:00:00 UTC+0'` the start of backtesting period
* `context.end = '2018-10-21T12:00:00 UTC+0'` the end of backtesting period
* `context.accounts = { account_name: {'id': exchange_name, 'balance': {currency1 : value, currency2: value}}}` Example values:
    * sds

## Run function
Run fuction executes identically in both backtester and live mode. It receives a historical or live exchange data point by point, 
and returns an **ACTION** or a list of **ACTION**s - a command for the Cryzen trading platform to place a buy/sell order, cancel previously placed but not filled order(s), a combination of those or nothing at all. 
### The data structure

## The ACTION